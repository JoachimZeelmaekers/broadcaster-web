import React from 'react';
import './App.css';
import Upload from "./components/upload/Upload";

function App() {
  return (
    <div className="app">
      <header className="app-header">
        <div className="card">
          <Upload />
        </div>
      </header>
    </div>
  );
}

export default App;
