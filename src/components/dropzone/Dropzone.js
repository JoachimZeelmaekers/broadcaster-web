import React, { useState, useRef } from "react";
import "./Dropzone.css";

export default function Dropzone({disabled, onFilesAdded: filesAdded}) {
  const [highlight, setHighlight] = useState(false);
  const fileInputRef = useRef();

  const openFileDialog = () => {
    if (disabled) return;
    fileInputRef.current.click();
  };

  const fileListToArray = (list) => {
    const array = [];
    for (let i = 0; i < list.length; i++) {
      array.push(list.item(i));
    }
    return array;
  };

  const onFilesAdded = (evt) => {
    if (disabled) return;
    const files = evt.target.files;
    if (filesAdded) {
      const array = fileListToArray(files);
      filesAdded(array);
    }
  };

  const onDragOver = (event) => {
    event.preventDefault();
    if (disabled) return;
    setHighlight(true);
  };

  const onDragLeave = (event) => {
    setHighlight(false);
  };

  const onDrop = (event) => {
    event.preventDefault();
    if (disabled) return;
    const files = event.dataTransfer.files;
    if (filesAdded) {
      const array = fileListToArray(files);
      onFilesAdded(array);
    }
    setHighlight(false);
  };

  return (
    <div
      className={`dropzone ${highlight ? "highlight" : ""}`}
      onDragOver={onDragOver}
      onDragLeave={onDragLeave}
      onDrop={onDrop}
      onClick={openFileDialog}
      style={{ cursor: disabled ? "default" : "pointer" }}
    >
      <input
        ref={fileInputRef}
        className="file-input"
        type="file"
        accept=".xlsx, .xls, .csv"
        multiple
        onChange={onFilesAdded}
      />
      <img
        alt="upload"
        className="icon"
        src="baseline-cloud_upload-24px.svg"
      />
      <span>Upload Excel file</span>
    </div>
  );
}
