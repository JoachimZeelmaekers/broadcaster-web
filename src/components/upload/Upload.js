import React, { useState, useMemo, useCallback } from 'react';
import Dropzone from '../dropzone/Dropzone';
import './Upload.css';
import Progress from '../progress/Progress';

const BASE_URL = 'https://europe-west1-oswald-saas.cloudfunctions.net/oswald-broadcaster';

export default function Upload() {
  const [files, setFiles] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [successfullyUploaded, setSuccessfullyUploaded] = useState(false);
  const [uploadProgress, setUploadProgress] = useState({});
  const [phoneNumber, setPhoneNumber] = useState('');
  const [audioFile, setAudioFile] = useState(false);
  const onFilesAdded = useCallback(
    (filesToAdd) => {
      setFiles(files.concat(filesToAdd));
    },
    [files]
  );

  const uploadFiles = useCallback(async () => {
    setUploadProgress({});
    setUploading(true);
    const promises = [];
    files.forEach((file) => {
      promises.push(sendRequest(file));
    });
    try {
      const result = await Promise.all(promises);
      console.log(await Promise.all(result.map((item) => item.json())));
      setUploadProgress({ state: 'done', percentage: 100 });
    } catch (e) {
      console.log(e);
    } finally {
      setSuccessfullyUploaded(true);
      setUploading(false);
      setAudioFile(false);
      setPhoneNumber('');
    }
  }, [files]);

  const sendRequest = (file) => {
    const formData = new FormData();
    formData.append('file', file, file.name);
    let url = `${BASE_URL}`;
    return fetch(url, {
      method: 'POST',
      body: formData,
    });
  };

  const renderProgress = useMemo(() => {
    if (uploading || successfullyUploaded) {
      return (
        <div className="progress-wrapper">
          <Progress progress={uploadProgress ? uploadProgress.percentage : 0} />
          <img
            className="check-icon"
            alt="done"
            src={'baseline-check_circle_outline-24px.svg'}
            style={{
              opacity: successfullyUploaded ? 0.5 : 0,
            }}
          />
        </div>
      );
    }
  }, [successfullyUploaded, uploadProgress, uploading]);

  const clearState = () => {
    setUploadProgress({});
    setUploading(false);
    setSuccessfullyUploaded(false);
    setFiles([]);
    setPhoneNumber('');
  };

  const testPhoneNumber = useCallback((to, audioFile) => {
    const url = `${BASE_URL}?phoneNumber=${to}`;
    return fetch(url, {
      method: 'POST',
      body: null,
    });
  }, []);

  const renderActions = useMemo(() => {
    if (successfullyUploaded) {
      return (
        <button className={'clear-button'} onClick={() => clearState()}>
          Clear
        </button>
      );
    } else {
      return (
        <>
          <div className={'action-wrapper'}>
            <div className={'input-wrapper'}></div>
            <div>
              <button className={'clear-button'} onClick={clearState}>
                Clear
              </button>
              <button disabled={files.length === 0 || uploading} onClick={uploadFiles}>
                Upload
              </button>
            </div>
          </div>
          <hr />
          <div className={'action-wrapper action-wrapper-bb'}>
            <div className={'input-wrapper'}>
              <label>Test phonenumber</label>
              <input type="text" value={phoneNumber} onChange={(event) => setPhoneNumber(event.target.value)} />
            </div>
            <div>
              <button
                disabled={!phoneNumber}
                onClick={() => {
                  testPhoneNumber(phoneNumber, audioFile);
                  clearState();
                }}
              >
                Try out
              </button>
            </div>
          </div>
        </>
      );
    }
  }, [successfullyUploaded, files.length, uploading, uploadFiles, phoneNumber, audioFile, testPhoneNumber]);

  return (
    <div className="upload">
      <div className="title-wrapper">
        <span className="title">Oswald Broadcaster</span>
        <img src={'orange.png'} alt="nothing" height="100" />
      </div>
      <div className="central-container">
        {files.length === 0 ? (
          <div className="content">
            <Dropzone onFilesAdded={onFilesAdded} disabled={uploading || successfullyUploaded} />
          </div>
        ) : (
          <div className="files">
            {files.map((file) => {
              return (
                <div key={file.name} className="row">
                  <span className="filename">{file.name}</span>
                  {renderProgress}
                </div>
              );
            })}
          </div>
        )}
      </div>
      <div className="actions">{renderActions}</div>
    </div>
  );
}
