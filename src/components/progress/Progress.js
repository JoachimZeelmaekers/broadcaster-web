import React from "react";
import "./Progress.css";

export default function Progress({progress}) {
  return (
    <div className="progress-bar">
      <div
        className="progress"
        style={{width: progress + "%"}}
      />
    </div>
  );
}
